# TestU01Repro-and-MTstatuses

## Reproducing Results

In the spirit of reproducible research, we found it pertinent to include a reproducibility section in this paper. The original idea to create such a section in a research paper, enhancing both the writing and reviewing processes, comes from Bajpai et al. (2017).

We used Bash scripts to run our experiments, with or without the Slurm Workload Manager (see Code 1).  
In the code repository, you will find 6 folders: `testU01-nodes`, `testU01-ru01`, `testU01-hpcsmp`, and three others with the same names but ending with `RealInterval`.  
Three of them contain the scripts and results for TestU01 on 32-bit integers, while the other three contain scripts and results for TestU01 on the \[0,1\] real interval.

Inside these folders, there are three subfolders corresponding to the three methods we examined: **MT Index status**, **Random spacing**, and **Sequence Splitting**.  
These subfolders contain the source code of TestU01 (directly taken from L’Ecuyer’s website) and the Mersenne Twister source code.  
Our code for executing the tests, named `testMT.c`, is based on examples from L’Ecuyer (e.g., `ex7.c`).  
Each experiment folder also includes a subfolder with the statuses used for the experiment:  
- `mtsSeed` for the 4096 statuses from the index sequence,  
- `mtsRand` for the 4096 statuses from random spacing, and  
- `mtsSeq` for the 4096 statuses from sequence splitting.

You can find all the result files from the Big Crush test in files named `resultStatusMts000M00XXXX` (from 0000 to 4095).  
To go further, we recommend attempting to reproduce our results on your own machine. 

## Reproduction Instructions

1. **Setup the Environment**  
   - Copy the source code of TestU01 and MT into another folder (this requires installing TestU01).  
   - You can find the full documentation on the [TestU01 website](http://simul.iro.umontreal.ca/testu01/tu01.html).  
   - To install TestU01, run the following commands:  
     ```bash
     ./configure
     make
     make install
     ```
   - If you do not have root privileges, run:  
     ```bash
     ./configure --prefix=<installDirectory>
     ```
   - Note: Installation is not mandatory if you simply copy our work folder, as the library is already installed in the `installDirectory`.  
     Just delete all result files with:  
     ```bash
     rm result*
     ```
     You will then have a folder with TestU01 and MT already installed.

2. **Set the Environment Variables**  
   Execute the following commands using the path to `installDirectory`.  
   (Use the `pwd` command in the `installDirectory` folder to get an absolute path, replacing `<installDirectory>` with the result of `pwd`):
   ```bash
   export LD_LIBRARY_PATH=<installDirectory>/lib:${LD_LIBRARY_PATH}
   export LIBRARY_PATH=<installDirectory>/lib:${LIBRARY_PATH}
   export C_INCLUDE_PATH=<installDirectory>/include:${C_INCLUDE_PATH}
    ```
3. **Compile the code**
Run the following command to compile:
```bash
gcc testMT.c mt19937ar.c -O2 -o executable -ltestu01 -lprobdist -lmylib -lm
```
4. **Execute the test**
Run your executable using:
```bash
./executable [path of the MT status to test] > resultFilename.txt
```
This will run the Big Crush test on your MT status, saving the output in a file named resultFilename.txt

